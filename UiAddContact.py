# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'UiAddContact.ui'
#
# Created: Fri Dec 14 00:43:44 2012
#      by: PyQt4 UI code generator 4.9.6
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_AddContactDialog(object):
    def setupUi(self, AddContactDialog):
        AddContactDialog.setObjectName(_fromUtf8("AddContactDialog"))
        AddContactDialog.resize(365, 127)
        self.buttonBox = QtGui.QDialogButtonBox(AddContactDialog)
        self.buttonBox.setGeometry(QtCore.QRect(10, 80, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.Nick = QtGui.QLineEdit(AddContactDialog)
        self.Nick.setGeometry(QtCore.QRect(10, 20, 341, 20))
        self.Nick.setObjectName(_fromUtf8("Nick"))
        self.Nick.setText(_fromUtf8("Nick"))
        self.Phone = QtGui.QLineEdit(AddContactDialog)
        self.Phone.setGeometry(QtCore.QRect(10, 50, 341, 20))
        self.Phone.setObjectName(_fromUtf8("Phone"))
        self.Phone.setText(_fromUtf8("Phone number (with country code)"))

        self.retranslateUi(AddContactDialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), AddContactDialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), AddContactDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(AddContactDialog)

    def retranslateUi(self, AddContactDialog):
        AddContactDialog.setWindowTitle(_translate("AddContactDialog", "Adding a new contact", None))

